"""Simple print-debugging library"""
import traceback
from pprint import pformat
from sys import stderr
from typing import Callable, TypeVar

__all__ = ("dbg",)

T = TypeVar("T")


def dbg(
    value: T,
    /,
    mapping: Callable[[T], str] = pformat,
    *,
    prefix: str = "Value: ",
) -> T:
    """Simple debug print

    :param value: value to print
    :param mapping: function to apply to value when printing
    :param prefix: String to print before the value

    :returns: `value`
    """
    print(
        "Call stack:\n",
        *traceback.format_stack()[:-1],
        prefix,
        "\n".join(
            l if i == 0 else " " * len(prefix) + l
            for i, l in enumerate(mapping(value).splitlines())
        ),
        sep="",
        file=stderr,
    )
    return value
