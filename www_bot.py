"""
www-bot
Copyright (C) 2020  John Meow

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from discord.ext import commands

from config import TOKEN

bot = commands.Bot("/")

bot.load_extension("jishaku")
bot.load_extension("cogs.www")

LICENSE_URL = "https://www.gnu.org/licenses/agpl-3.0.ru.html"

LICENSE = f"""Лицензия, действующая на код бота: {LICENSE_URL}
Лицензия, действующая на вопросы: https://db.chgk.info/copyright
Все вопросы скачиваются с сайта https://db.chgk.info/
Исходный код бота находится на GitLab: https://gitlab.com/johnmeow/www-bot
"""


@bot.event
async def on_ready():
    print("Ready!")


@bot.command()
async def license(ctx: commands.Context):
    """Отправляет лицензию, действующую на бота и вопросы"""
    return await ctx.send(LICENSE)


def main():
    bot.run(TOKEN)
